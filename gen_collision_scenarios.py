from gen_convex_shape import *


#where the generated objects will be stored
OUTPUT_FILE = "./objects"
EPS = 0.01

from enum import Enum
class Scenario(Enum):
    NO_COLLISION = 0
    NEAR_COLLISION = 1
    CLOSE_COLLISION = 2
    COLLISION = 3

from qp import solve_lp, quadprog_solve_qp
#~ np.set_printoptions(formatter={'float': lambda x: "{0:0.1f}".format(x)})

    
def translate(obj, translation):
    translatedVerts = [translation + el for el in obj["vertices"]]
    return genObjectFromPoints(translatedVerts)
    
#computes the minimum translation such that the object is completly on one side of the plane
def minTranslationToSeparatingPlane(obj, plane, above=True, epsilon = 0.):
    verts = obj["vertices"]
    sign = 1.
    if above:
        sign = -1.
    #n.dot(p + x) <= d - epsilon
    #n.dot(x) <= d - n.dot(p) - epsilon
    # if above
    #-n.dot(x) <= -d + n.dot(p) - epsilon
    n = plane[:3]
    nDn = n.dot(n)
    nVerts = len(verts)
    A = zeros((nVerts,3)); 
    b = ones(A.shape[0]) * plane[-1]
    for i, p in enumerate(verts):
        A[i,:3] = n
        b[i] -= n.dot(p)
    A *= sign
    b = b * sign - ones(b.shape) * epsilon
    q = zeros(3)
    H = identity(3)
    return quadprog_solve_qp(H,q,A,b).x


#Given two objects, put them in a state such that they are separated by a plane, yet 
# two points are really close
def moveArbitrarilyClose(obj1,obj2,plane):
    
    def alignOnSidePlane(obj,above):
        translation = minTranslationToSeparatingPlane(obj,plane,above=above)
        return translate(obj, translation)
        
    def closestPointsVector(o1, o2):
        pts1 = o1["vertices"]
        pts2 = o2["vertices"]
        bestDist = 10000.
        trVectorForO1 = None
        for pt1 in pts1:
            for pt2 in pts2:
                dist = np.linalg.norm(pt2 - pt1)
                if dist < bestDist:
                    trVectorForO1 = pt2 - pt1
                    bestDist = dist
        return trVectorForO1
                     
    obj1Tr = alignOnSidePlane(obj1, False)   
    obj2Tr = alignOnSidePlane(obj2, True)   
        
    #translate obj1Tr so that two points almost coincide without collision
    trVectorForO1 = closestPointsVector(obj1Tr, obj2Tr)
    translatedVerts = [trVectorForO1 + el for el in obj1Tr["vertices"]]
    obj1Tr = genObjectFromPoints(translatedVerts)
    return obj1Tr, obj2Tr



def assertCollision(obj1, obj2, shouldCollide):
    A1, b1 = ineqQHull(obj1["hull"])
    A2, b2 = ineqQHull(obj2["hull"])
    A = vstack([A1,A2])
    b = np.concatenate([b1,b2])
    col = quadprog_solve_qp(identity(3),zeros(3),A,b).success
    return col and shouldCollide or (not col and not shouldCollide)

#generate two objects not in collision, but such that if obj1 is slightly translated
#along the plane normal a collision occurs
def generateTwoObjectsClose(numPoints1, scaleFactor1, numPoints2, scaleFactor2):
    obj1 = genObject(numPoints = numPoints1, scaleFactor = scaleFactor1)
    obj2 = genObject(numPoints = numPoints2, scaleFactor = scaleFactor2)
    plane = genRandomPlane()
    obj1Tr, obj2Tr = moveArbitrarilyClose(obj1,obj2,plane)
    return obj1Tr, obj2Tr, plane

# generate a scenario where two objects are close but not in collision
def generateNearCollisionScenario(numPoints1 = 20, scaleFactor1 = 1., numPoints2 = 20, scaleFactor2 = 1.):
    colOk = False
    while not colOk:
        obj1, obj2, plane = generateTwoObjectsClose(numPoints1, scaleFactor1, numPoints2, scaleFactor2)
        #translate a bit obj towards the opposite of the normal to guarantee no collision occurs
        obj1 = translate(obj1,  -  EPS * plane[:-1])
        colOk = assertCollision(obj1, obj2, shouldCollide = False)
        if not colOk:
            print("In generateNearCollisionScenario: collision detected and should not happen, retrying") 
            # ~ ax = plot_hull(obj1["hull"])
            # ~ ax = plot_hull(obj2["hull"],ax = ax, color = "b")
            # ~ plt.show(block=True)
    return {"Case" : Scenario.NEAR_COLLISION, "obj1" : obj1, "obj2" : obj2 }
    
def generateCloseCollisionScenario(numPoints1 = 20, scaleFactor1 = 1., numPoints2 = 20, scaleFactor2 = 1.):    
    colOk = False
    while not colOk:
        obj1, obj2, plane = generateTwoObjectsClose(numPoints1, scaleFactor1, numPoints2, scaleFactor2)
        #translate a bit obj towards the normal to guarantee collision occurs
        obj1 = translate(obj1,   EPS * plane[:-1])
        colOk = assertCollision(obj1, obj2, shouldCollide = True)
        if not colOk:
            print("In generateCloseCollisionScenario: collision not detected and should happen, retrying") 
            ax = plot_hull(obj1["hull"])
            ax = plot_hull(obj2["hull"],ax = ax, color = "b")
            plt.show(block=True)
    return {"Case" : Scenario.CLOSE_COLLISION, "obj1" : obj1, "obj2" : obj2 }
    
def generateCollisionScenario(numPoints1 = 20, scaleFactor1 = 1., numPoints2 = 20, scaleFactor2 = 1.):
    colOk = False
    while not colOk:
        obj1, obj2, plane = generateTwoObjectsClose(numPoints1, scaleFactor1, numPoints2, scaleFactor2)
        #translate a bit obj towards the normal to guarantee collision occurs
        obj1 = translate(obj1,  0.1 * scaleFactor1 * plane[:-1])
        colOk = assertCollision(obj1, obj2, shouldCollide = True)
        if not colOk:
            print("In generateCollisionScenario: collision not achieved, retrying")
    return {"Case" : Scenario.COLLISION, "obj1" : obj1, "obj2" : obj2 }
    
def generateNoCollisionScenario(numPoints1 = 20, scaleFactor1 = 1., numPoints2 = 20, scaleFactor2 = 1.):
    obj1, obj2, plane = generateTwoObjectsClose(numPoints1, scaleFactor1, numPoints2, scaleFactor2)
    translationAbove = minTranslationToSeparatingPlane(obj1,plane,above=True)
    obj1 = translate(obj1,  translationAbove)    
    translationUnder = minTranslationToSeparatingPlane(obj2,plane,above=False)
    obj1 = translate(obj2,  translationUnder)
    if not assertCollision(obj1, obj2, shouldCollide = False):
        raise ValueError("In generateNoCollisionScenario: collision detected and should not happen") 
    return {"Case" : Scenario.NO_COLLISION, "obj1" : obj1, "obj2" : obj2 }
    
def ineqQHull(hull):
    A = hull.equations[:,:-1]
    b = -hull.equations[:,-1]
    return A,b
    
    
##################### DRAW STUFF ######################

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def drawScenario(scenario, plane = None, scaleFactor = 10.):
    ax = plot_hull(scenario["obj1"]["hull"])
    ax = plot_hull(scenario["obj2"]["hull"],ax = ax, color = "b")
    if plane is not None:
        ax = plot_plane(plane,ax = ax, scaleFactor = scaleFactor)
    plt.show(block=False)

################################# MAIN ##############################"

if __name__ == "__main__":    
    #1. Generate an object, a random plane that goes through 0,
    # then find the minimum translation that will put exactly the object below the plane
    # then the one that will put it above the plane and plot
    drawScenario(generateNoCollisionScenario())
    drawScenario(generateNearCollisionScenario())
    drawScenario(generateCloseCollisionScenario())
    drawScenario(generateCollisionScenario())
    
    nPoints = 1000
    data = [generateNoCollisionScenario(numPoints1 = nPoints, numPoints2 = nPoints) for _ in range(10)]
    # ~ [generateNearCollisionScenario(numPoints1 = nPoints, numPoints2 = nPoints) for _ in range(100)]
    # ~ [generateCloseCollisionScenario(numPoints1 = nPoints, numPoints2 = nPoints) for _ in range(100)]
    # ~ data = [generateCollisionScenario(numPoints1 = nPoints, numPoints2 = nPoints) for _ in range(1000)]

    #test saving large non collision first
    

    import pickle
    with open(OUTPUT_FILE, 'wb') as fid:
        pickle.dump(data, fid, 2)
