


import numpy as np
from numpy import array, asmatrix, matrix, zeros, ones
from numpy import array, dot, stack, vstack, hstack, asmatrix, identity, cross, concatenate
from numpy.linalg import norm
from numpy.random import rand 
from random import randint



#points will be sampled around 2m cube centered on 0
BOUNDING_BOX = [[-1.,-1,-1],[1,-1,-1],[1,1,-1],[-1,1,-1],[-1.,-1,1],[1,-1,1],[1,1,1],[-1,1,1]]
BOUNDING_BOX = [array(el) for el in BOUNDING_BOX]

#not the number of vertices, rather the number of points sampled before computing the convex hull
OBJECT_NUM_VERTICES_RANGE = [(5,15), (30,50), (100,200), (1000, 1500)]
NUM_OBJECTS_PER_TYPE = 10

#where the generated objects will be stored
OUTPUT_FILE = "/media/data/dev/linux/hpp/src/collision/objects"

try:
    import bpy
    import bmesh
    
    #generate numPoints points within BOUNDING_BOX, scaled by scaleFactor
    def genPointCloud(numPoints, scaleFactor):
        currentBB = [el * scaleFactor for el in BOUNDING_BOX]
        nSourcePoints = len(currentBB)
        #random convex combination of bounding box points gives a point uniformely sampled in the box
        def convexCombination():
            sampled = rand(nSourcePoints)
            convexSample = [el / sum(sampled) for el in sampled]
            return sum([w * el  for (w,el) in zip(convexSample, currentBB)])
        #each point is a convex combination drawn 
        return[convexCombination() for _ in range(numPoints)]


    def objFromPointArray(verts):
        mesh = bpy.data.meshes.new("mesh")  # add a new mesh
        obj = bpy.data.objects.new("MyObject", mesh)  # add a new object using the mesh

        scene = bpy.context.scene
        #put the object into the scene (link)
        # BLENDER < 2.8
        # ~ scene.objects.link(obj)    
        # ~ scene.objects.active = obj  # set as the active object in the scene
        # ~ obj.select = True  # select object
        # BLENDER >= 2.8
        bpy.context.collection.objects.link(obj) 
        bpy.context.view_layer.objects.active = obj
        obj.select_set(True)

        mesh = bpy.context.object.data
        bm = bmesh.new()

        for v in verts:
            bm.verts.new(v)  # add a new vert
        # make the bmesh the object's mesh
        bm.to_mesh(mesh)  
        bm.free()  # always do this when finished
        return obj

    def convexHull():    
        bpy.ops.object.editmode_toggle()
        bpy.ops.mesh.delete( type='EDGE_FACE')
        bpy.ops.mesh.select_mode(type="VERT")
        bpy.ops.mesh.select_all(action = 'SELECT')
        bpy.ops.mesh.convex_hull()
        bpy.ops.object.editmode_toggle()

    def triangulate(obj):
        me = obj.data
        # Get a BMesh representation
        bm = bmesh.new()
        bm.from_mesh(me)
        bmesh.ops.triangulate(bm, faces=bm.faces[:])
        # Finish up, write the bmesh back to the mesh
        bm.to_mesh(me)
        bm.free()


    #return a convex 3D polyhedron, computed as the convex hull
    #of numPoints randomly sampled in BOUNDING_BOX, scaled by scaleFactor
    def genObject(numPoints = 10, scaleFactor = 1):
        verts = genPointCloud(numPoints, scaleFactor)
        obj = objFromPointArray(verts)       
        convexHull()
        triangulate(obj)
        return obj
       
    def getObjectVerticesAndTriangles(obj):
        #get vertices
        W = obj.matrix_world
        vertices = [W @ el.co for el in obj.data.vertices]
        vertices = [array([el[0],el[1],el[2]]) for el in vertices]    
        triangles = [[el for el in polygon.vertices] for polygon in obj.data.polygons]    
        return {"vertices" : vertices, "triangles" : triangles}
            



    def createBlenderPlane(plane):
        points = [[-10,-10], [10,-10], [-10,10], [10,10]]
        vertices = [(pt[0], pt[1], plane[3] - plane[0] * pt[0] - plane[1] * pt[1]) for pt in points]
        objFromPointArray(vertices)
        convexHull()



    #generate random separating plane that contains 0
    def genRandomPlane():
        plane = rand(4)
        plane[-1] = 0.
        plane /= np.linalg.norm(plane)
        createBlenderPlane(plane)
        return plane
        



    #~ objs = [genObject(randint(5,100),rand() * 10.) for _ in range(2)]
    #~ objs = [genObject(randint(5,100),rand() * 10.) for _ in range(2)]

    objs = [genObject(randint(5,100),rand() * 10.) for i in range(1)]
    plane = genRandomPlane()
    #~ res = minTranslationToSeparatingPlane(obj, plane, above=True)
    #~ print("res ", plane[:-1] * res)

    data = [[getObjectVerticesAndTriangles(obj) for obj in objs], plane ]

    import pickle
    with open(OUTPUT_FILE, 'wb') as fid:
        pickle.dump(data, fid, 2)

except ImportError:
    pass


