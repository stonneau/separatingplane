import numpy as np
from numpy import array, asmatrix, matrix, zeros, ones
from numpy import array, dot, stack, vstack, hstack, asmatrix, identity, cross, concatenate
from numpy.linalg import norm
from numpy.random import rand 
from random import randint

from scipy.spatial import qhull
from scipy.spatial import ConvexHull

#points will be sampled around 2m cube centered on 0
BOUNDING_BOX = [[-1.,-1,-1],[1,-1,-1],[1,1,-1],[-1,1,-1],[-1.,-1,1],[1,-1,1],[1,1,1],[-1,1,1]]
BOUNDING_BOX = [array(el) for el in BOUNDING_BOX]
#not the number of vertices, rather the number of points sampled before computing the convex hull
OBJECT_NUM_VERTICES_RANGE = [(5,15), (30,50), (100,200), (1000, 1500)]
NUM_OBJECTS_PER_TYPE = 10


    
#generate numPoints points within BOUNDING_BOX, scaled by scaleFactor
def genPointCloud(numPoints, scaleFactor):
    currentBB = [el * scaleFactor for el in BOUNDING_BOX]
    nSourcePoints = len(currentBB)
    #random convex combination of bounding box points gives a point uniformely sampled in the box
    def convexCombination():
        sampled = rand(nSourcePoints)
        convexSample = [el / sum(sampled) for el in sampled]
        return sum([w * el  for (w,el) in zip(convexSample, currentBB)])
    #each point is a convex combination drawn 
    return[convexCombination() for _ in range(numPoints)]

def convexHull(pts):    
    return ConvexHull(array(pts), qhull_options='Qt') #Qt triangulates
   
def getObjectVerticesAndTriangles(hull):
    keptIndices = hull.vertices.tolist()
    filteredPoints = [hull.points[i] for i in keptIndices]
    
    def mapVerticeListToFilteredPoints():
        simplicesIdx = hull.simplices.tolist() 
        return [[keptIndices.index(el) for el in simplex] for simplex in simplicesIdx]
        
    return {"vertices" : filteredPoints, "triangles" : mapVerticeListToFilteredPoints(), "hull" : hull}

#return a convex 3D polyhedron, computed as the convex hull
#of numPoints randomly sampled in BOUNDING_BOX, scaled by scaleFactor
def genObject(numPoints = 10, scaleFactor = 1):
    verts = genPointCloud(numPoints, scaleFactor)
    hull = convexHull(verts)
    return getObjectVerticesAndTriangles(hull)
    
#return a convex 3D polyhedron, computed as the convex hull
#of numPoints randomly sampled in BOUNDING_BOX, scaled by scaleFactor
def genObjectFromPoints(verts):
    hull = convexHull(verts)
    return getObjectVerticesAndTriangles(hull)

#generate random separating plane that contains 0
# returns a vector [a,b,c,d] such that a x + by + cz = d is the plane equation
def genRandomPlane():
    plane = rand(4)
    plane[-1] = 0.
    plane /= np.linalg.norm(plane)
    return plane
    


##################### DRAW STUFF ######################

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def plot_hull_in_subplot(hull, ax, color = "r"):    
    apts = hull.points
    for s in hull.simplices:
        s = np.append(s, s[0])  # Here we cycle back to the first coordinate
        ax.plot(apts[s, 0], apts[s, 1], apts[s, 2], color+"-")


def plot_hull(hull, color = "r", ax = None):
    if ax is None:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection="3d")
    plot_hull_in_subplot(hull, ax, color)
    return ax


def plot_plane(plane, scaleFactor, ax = None):
    if ax is None:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection="3d")
    # create x,y
    rg = (int)(scaleFactor)
    xx, yy = np.meshgrid(range(-rg,rg), range(-rg,rg))
    # calculate corresponding z
    z = (-plane[0] * xx - plane[1] * yy - plane[3]) * 1. /plane[2]
    ax.plot_surface(xx, yy, z, alpha=0.2)
    return ax

################################# MAIN ##############################"

#1. Generate an object, a random plane that goes through 0,
# then find the minimum translation that will put exactly the object below the plane
# then the one that will put it above the plane and plot


if __name__ == "__main__":

    scale = 10
    obj = genObject(numPoints = 50, scaleFactor = scale)
    ax = plot_hull(obj["hull"])
    plot_plane(genRandomPlane(), scale / 2., ax)
    plt.show(block=False)
