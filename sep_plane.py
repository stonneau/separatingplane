from gen_collision_scenarios import *

from qp import solve_least_square

I = identity(4)
zeros4 = zeros(4)
    
#if the only solution is a plane perfectly orthogonal to the planesacrificiedPlane the collision detection will always find a collision
sacrificedPlane = array([0.1,0.2,1.])
sacrificedPlane = sacrificedPlane / np.linalg.norm(sacrificedPlane)

def separatingPlane(objectPointList):
    #find plane such that obj1 is below plane
    #and obj2 is above
    #unknowns are a b c d
    # \for all p in obj1,  a p_x + b p_y + c p_z - d <= 0  
    # \for all p in obj2, -a p_x - b p_y - c p_z + d <= 0  

    
    t1 = time.clock()
    size1 = len(objectPointList[0])
    size2 = len(objectPointList[1])
    A = ones((size1+size2+1,4))
    b = zeros(size1+size2+1)
    A[:size1,:-1] = objectPointList[0]
    A[size1:-1,:-1] = objectPointList[1]
    A[size1:-1]    *= -1
    A[-1,:-1] = -sacrificedPlane
    b[-1] = -0.001    
    return quadprog_solve_qp(I,zeros4,A,b)
    
def fclObj(obj): 
    verts = hppfcl.StdVec_Vec3f ()
    faces = hppfcl.StdVec_Triangle ()
    verts.extend( obj["vertices"])
    [faces.append(hppfcl.Triangle(el[0],el[1],el[2]))  for el in obj["triangles"] ]
    return hppfcl.Convex(verts, faces)

def gjk(s1,s2, tf1, tf2):
    guess = np.array([1., 0., 0.])
    support_hint = np.array([0, 0], dtype=np.int32)

    shape = hppfcl.MinkowskiDiff()
    t1  = time.clock()
    shape.set(s1, s2, tf1, tf2)
    gjk = hppfcl.GJK(100, 1e-6)
    status = gjk.evaluate(shape, guess, support_hint)
    t2  = time.clock()
    print (timMs(t1,t2))

################################# MAIN ##############################"

if __name__ == "__main__":    
    
    import pickle
    f = open(OUTPUT_FILE, 'rb') 
    data = pickle.load(f)
    f.close()
    
    # ~ scen = data[3]
    
    import time
    def timMs(t1, t2):
        return (t2-t1) * 1000.
    
    
    lpData = [(scen["obj1"]["vertices"],scen["obj2"]["vertices"]) for scen in data]
    t1  = time.clock()
    # ~ res = [separatingPlane(el) for el in lpData]
    res = separatingPlane(lpData[0])
    t2  = time.clock()
    # ~ drawScenario(scen, res.x, scaleFactor = 2.)
    print (timMs(t1,t2))


    import hppfcl
    scen    = data[0]
    fclObjs = [ (fclObj(scen["obj1"]), fclObj(scen["obj2"])) for scen in data]

    cReq = hppfcl.CollisionRequest() 
    cRes = hppfcl.CollisionResult() 
    # ~ hppfcl.collide(fclObjs[0][0],fclObjs[0][1],cReq,cRes) 
    
    o1 = fclObjs[0][0]
    o2 = fclObjs[0][1]
    
    t1 = hppfcl.Transform3f()
    t2 = hppfcl.Transform3f()
    
    gjk(o1,o2,t1,t2)
    
    # ~ rq.num_max_contacts = 0 

    # ~ import pickle
    # ~ with open(OUTPUT_FILE, 'wb') as fid:
        # ~ pickle.dump(data, fid, 2)
